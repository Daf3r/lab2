import './bootstrap';
import { createApp } from 'vue';
import ExampleComponent from './components/ExampleComponent.vue';
import FormComponent from './components/FormComponent.vue'
import DataComponent from './components/DataComponent.vue'

const app = createApp({});

app.component('example-component', ExampleComponent);
app.component('form-component', FormComponent);
app.component('data-component', DataComponent);


app.mount('#app');
